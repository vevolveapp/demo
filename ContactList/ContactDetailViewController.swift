//
//  ContactDetailViewController.swift
//  ContactList
//
//  Created by webwerks on 22/01/20.
//  Copyright © 2020 hitesh. All rights reserved.
//

import UIKit
import CoreData
enum type {
    case new
    case edit
    case delete
}
class ContactDetailViewController: UIViewController {
    @IBOutlet weak var barButton: UIButton!
    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var enailTextField: UITextField!
    var vcType:type = type.new
    var user : User?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        switch vcType {
        case .edit:
            barButton.setTitle("Update", for: UIControl.State.normal)
            initContact()
        case .new:
            barButton.setTitle("Save", for: UIControl.State.normal)
        case .delete:
            barButton.setTitle("Delete", for: UIControl.State.normal)
            initContact()
        }
    }
    func initContact(){
        guard let contact=user else {
            return
        }
        self.enailTextField.text = contact.email ?? ""
        self.nameTextField.text = contact.name ?? ""
        self.phoneTextField.text = contact.phone ?? ""
        if let imageData: Data =  contact.image
        {
        let image = UIImage.init(data:imageData as Data)
        self.imageview.image = image
        }
    }
    @IBAction func barButtonAction(_ sender: Any) {
        switch vcType {
        case .edit:
            editUser()
        case .new:
            saveUser()
        case .delete:
            deleteUser()
        }
    }
    func editUser()
    {
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext =
          appDelegate.persistentContainer.viewContext
        user?.setValue(nameTextField.text, forKeyPath: "name")
        user?.setValue(phoneTextField.text, forKeyPath: "phone")
        user?.setValue(enailTextField.text, forKeyPath: "email")
        let imageData = imageview.image!.jpegData(compressionQuality: 1.0)
        user?.setValue(imageData, forKeyPath: "image")
        do {
          try managedContext.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        self.navigationController?.popViewController(animated: true)
    }
    func saveUser()
    {
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext =
          appDelegate.persistentContainer.viewContext
        let entity =
          NSEntityDescription.entity(forEntityName: "User",
                                     in: managedContext)!
        user = (NSManagedObject(entity: entity,
                                insertInto: managedContext) as! User)
        user?.setValue(nameTextField.text, forKeyPath: "name")
        user?.setValue(phoneTextField.text, forKeyPath: "phone")
        user?.setValue(enailTextField.text, forKeyPath: "email")
        let imageData = imageview.image!.jpegData(compressionQuality: 1.0)
        user?.setValue(imageData, forKeyPath: "image")
        
    
        do {
          try managedContext.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        self.navigationController?.popViewController(animated: true)
    }
    func deleteUser()
    {
        guard let contact=user else {
                   return
               }
        guard let appDelegate =
          UIApplication.shared.delegate as? AppDelegate else {
          return
        }
        let managedContext =
          appDelegate.persistentContainer.viewContext
        managedContext.delete(contact)
        do {
          try managedContext.save()
        } catch let error as NSError {
          print("Could not save. \(error), \(error.userInfo)")
        }
        self.navigationController?.popViewController(animated: true)
    }
}
